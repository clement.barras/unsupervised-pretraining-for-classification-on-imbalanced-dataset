import matplotlib.pyplot as plt
import numpy as np
import torch
from PIL import Image
from torchvision import transforms as trans


class Dataset(torch.utils.data.Dataset):

    def __init__(self, cifarset, base_idx=None, prop={}, resample=True, transforms=trans.ToTensor()):
        """Constructor.
        
        Parameters
        ----------
        cifarset : torchvision.datasets.CIFAR10 instance
            Original CIFAR10 dataset. See https://pytorch.org/docs/stable/torchvision/datasets.html#cifar.
            Transforms passed to its constructor are not applied by this object.
        base_idx : list or array-like of int, optional
            Specifies the indexes of the images to keep from cifarset, useful for creating subset.
            Default is None, which corresponds to no subsetting.
        prop : dict {class_name: float in [0, 1]}, optional
            Specifies the proportions of images to keep for each class. If a class name is missing from `prop`,
            its proportion will be considered to be 1.
            Default is {} : all images are kept.
            Example: : {"bird":0.5, "deer":0.5, "truck":0.5}
        resample : bool, optional
            Decides if oversample classes with proportions below 1. If True, elements of these classes are
            sampled with repetition such that there are len(cifarset)//len(classes) of them in the created dataset,
            and `prop` becomes the proportions of UNIQUE images of each class.
        transforms : torchvision.tranforms, optional
            Transformations to apply to the images.
        """        
        super(Dataset, self).__init__()

        if base_idx is None:
            base_idx = np.arange(len(cifarset))
        targets = np.array(cifarset.targets)

        self.cifar_idx = [] # we select samples to keep via their indexes in the input cifarset
        for class_name, i in cifarset.class_to_idx.items():
            class_idx = base_idx[targets[base_idx] == i] # indexes of all images of class i
            class_count = int(prop[class_name] * len(class_idx)) if class_name in prop else len(class_idx) #number of images to keep for class i
            class_idx = np.random.choice(class_idx, size=class_count, replace=False) # list of images to keep
            if resample and (class_name in prop) and (prop[class_name] < 1): # oversampling
                class_idx = np.random.choice(class_idx, size=len(base_idx) // 10, replace=True)
            self.cifar_idx.append(class_idx)

        self.cifar_idx = np.concatenate(self.cifar_idx)
        np.random.shuffle(self.cifar_idx) # shuffle image indexes

        self.data = cifarset.data[self.cifar_idx]
        self.targets = torch.tensor(targets[self.cifar_idx], dtype=torch.long)
        self.classes = tuple(cifarset.class_to_idx.keys())
        self.class_to_idx = cifarset.class_to_idx
        self.transforms = transforms
        self.imgs = [Image.fromarray(ar) for ar in self.data] # transform to PIL images

    def __getitem__(self, i):
        img = self.imgs[i]
        if self.transforms is not None:
            img = self.transforms(img)
        return {"data": img, "target": self.targets[i]}

    def __len__(self):
        return len(self.data)

    def plot(self, i):
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        ax.imshow(self.data[i])
        ax.set_title(self.classes[self.targets[i]])
        ax.axis('off')
