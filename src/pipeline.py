import json
from os.path import join

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision.datasets import CIFAR10

from .datasets import Dataset
from .utils import plot_grad_flow, corrupt_img, freeze, unfreeze, create_directory


class Pipeline:

    def __init__(self, config, encoder, decoder=None, classifier=None, seed=0, diagnose=False):
        """Constructor. After instancing a Pipeline object, set_mode must be called before starting
        anything.
        
        Parameters
        ----------
        config : dict
            Contains the pipeline's parameters. Must contain the keys:
            - "transform_train": transformations used in the train set
            - "transform_test": transformations used in the validation and test set
            - "train_bs": batch size while training
            - "test_bs": batch size while validating/testing
            - "lr": initial learning rate
            - "train_prop": dictionary of proportions of samples to keep for each class (see datasets.py)
            - "noise": std of the gaussian noise applied to input
            - "masking": proportion of pixel to randomly turn off
            - "l1_pen": coefficient of l1 penalization on the hidden layer
        encoder : Encoder object (see networks/encoders.py)
        decoder : Decoder object (see networks/decoders.py), optional
        classifier : Classifier object (see networks/classifiers.py), optional
        seed : int, optional
            For setting the seed to reproduce results, by default 0
            In practice, it works for keeping the same datasets accross trainings,
            but trainings are still a bit random...
        diagnose : bool, optional
            To plot gradients distribution at each epoch or not
        """

        self.config = config
        self.seed = seed
        np.random.seed(seed)
        torch.manual_seed(seed)
        self.diagnose = diagnose
        self.device = torch.device('cuda') if torch.cuda.is_available() else 'cpu'
        # setting up pipeline elements
        self.trainset = self.valset = self.testset = None
        self.trainloader = self.valloader = self.testloader = None
        self.optimizer = None
        self.scheduler = None
        self.noise = 0 if "noise" not in config else config["noise"]
        self.masking = 0 if "masking" not in config else config["masking"]
        self.l1_pen = 0 if "l1_pen" not in config else config["l1_pen"]
        self.train_prop = {} if "train_prop" not in config else config["train_prop"]
        self.task = None
        self.has_valset = None
        self.mode_set = False  # flag controlling if set_mode has been called successfully yet
        # setting up networks
        self.encoder = encoder.to(self.device)
        self.model = nn.ModuleDict()
        self.model["encoder"] = self.encoder
        if decoder is not None:
            self.decoder = decoder.to(self.device)
            self.model["decoder"] = self.decoder
        if classifier is not None:
            self.classifier = classifier.to(self.device)
            self.model["classifier"] = self.classifier

    def set_mode(self, active, task, corrupt, has_valset):
        """Sets the pipeline mode: which sub-networks are active, for which task to train them, if input data
        should be corrupted or not and if a validation set should be included.
        Creates optimizer and datasets internally.
        
        Parameters
        ----------
        active : list of elements of ("encoder", "decoder", "classifier")
            Controls which sub-networks should be active. Other networks are frozen.
        task : string
            Can be "ae" (train for autoencoding task), "classification" or "both" (train for both tasks
            simultaneously).
        corrupt : bool
            Determines if the input data should be corrupted.
        has_valset : bool
            Determines if a validation set should be included.
        """
        print("\n")
        for k, v in self.model.items():
            if k in active:
                print(f"{k} net: activated")
                unfreeze(v)
            else:
                print(f"{k} net: deactivated")
                freeze(v)
        # sets a new optimizer here, so it is reset every time (needed anyway when networks are frozen,
        # and for a fresh start when changing tasks)
        self.optimizer = optim.Adam(filter(lambda p: p.requires_grad, self.model.parameters()),
                                    lr=self.config["lr"], weight_decay=1e-4)
        # self.optimizer = optim.SGD(filter(lambda p: p.requires_grad, self.model.parameters()),
        #                           lr=self.config["lr"], weight_decay=1e-4, momentum=0.9, nesterov=True)
        # self.scheduler = torch.optim.lr_scheduler.CyclicLR(self.optimizer, base_lr=self.config["lr"],
        #                                                   max_lr=10 * self.config["lr"])
        self.task = task
        if corrupt:
            self.noise = self.config["noise"]
            self.masking = self.config["masking"]
        else:
            self.noise = 0
            self.masking = 0
        print(f"masking ratio: {self.masking}\n"
              f"noise magnitude: {self.noise}")

        self.has_valset = has_valset
        self._prepare_sets(self.config["transform_train"], self.config["transform_test"],
                           self.config["train_bs"], self.config["test_bs"], has_valset)
        self.mode_set = True

    def _prepare_sets(self, transform_train, transform_test, train_bs, test_bs, has_valset):
        # internally initialize datasets and loaders
        print('==> Preparing data...')

        # original CIFAR10 datasets from torchvision.datasets
        cifar_trainset = CIFAR10(root='./data/train_data', train=True, download=True)
        cifar_testset = CIFAR10(root='./data/test_data', train=False, download=True)
        n_cifar = len(cifar_trainset)

        if has_valset:
            # randomly selects 80% of the image for training, keeps the remaining for validation.
            train_idx = np.random.choice(n_cifar, int(n_cifar * 0.8), replace=False)
            val_idx = np.setdiff1d(np.arange(n_cifar), train_idx)
            self.trainset = Dataset(cifar_trainset, base_idx=train_idx, transforms=transform_train,
                                    prop=self.train_prop)
            self.trainloader = torch.utils.data.DataLoader(self.trainset, batch_size=train_bs,
                                                           pin_memory=True, num_workers=0)
            # pin_memory for faster loadings to the GPU.
            self.valset = Dataset(cifar_trainset, base_idx=val_idx, transforms=transform_test, prop=self.train_prop)
            self.valloader = torch.utils.data.DataLoader(self.valset, batch_size=test_bs,
                                                         pin_memory=True, num_workers=0)
        else:
            self.trainset = Dataset(cifar_trainset, base_idx=np.arange(n_cifar), transforms=transform_train,
                                    prop=self.train_prop)
            self.trainloader = torch.utils.data.DataLoader(self.trainset, batch_size=train_bs,
                                                           pin_memory=True, num_workers=0)
            self.valset = self.valloader = None

        self.testset = Dataset(cifar_testset, transforms=transform_test)
        self.testloader = torch.utils.data.DataLoader(self.testset, batch_size=test_bs, pin_memory=True,
                                                      num_workers=0)

    def fit(self, n_epochs, save_dir=None, save_nets=[]):
        """Fit a model a certain number of epochs and optionally save training history and networks.
        Saved networks are the ones with the best validation performances through the epochs if has_valset is True.
        Otherwise, the networks of the last epoch are saved.
        
        Parameters
        ----------
        n_epochs : int
        save_dir : string (path), optional
            If specified, save a csv file containing training statistics throughout the epochs and a JSON info file.
        save_nets : list of elements of ("encoder", "decoder", "classifier"), optional
            If save_dir is also specified, list of networks to save.
        """
        if not self.mode_set:
            print("Pipeline mode not set. Please set it using the method set_mode.")

        if save_dir is not None:
            create_directory(save_dir)
        # keep tracks of loss and accuracies during training
        train_losses = []
        val_losses = []
        # keeps track of the best loss throughout the epochs
        best_loss = 1e12
        best_epoch = -1
        # which loss to consider for chosing the best epoch
        criterion = "loss_ae" if (self.task == "ae") else "loss_classif"

        for epoch in range(1, n_epochs + 1):
            print(f"\nEpoch #{epoch}")
            train_losses.append(self._train())  # training step
            if self.has_valset:
                val_loss = self._validation()  # validation step
                val_losses.append(val_loss)
                if (save_dir is not None) and (val_loss[criterion] < best_loss):  # saves the model if current best.
                    best_loss = val_loss[criterion]
                    best_epoch = epoch
                    for net in save_nets:
                        torch.save(self.model[net].state_dict(), join(save_dir, f"{net}.pth"))
                if (epoch == n_epochs):
                    print(f"\nBest epoch: {best_epoch},"
                          f"\ntrain results: {train_losses[best_epoch - 1]},"
                          f"\nvalidation results: {val_losses[best_epoch - 1]}")
            else:  # if no validation set, keep the last networks
                if (save_dir is not None) and (epoch == n_epochs):
                    for net in save_nets:
                        torch.save(self.model[net].state_dict(), join(save_dir, f"{net}.pth"))

        ## After training, write info files in save_dir if specified
        if save_dir is not None:
            # write a csv file containing training statistics throughout the epochs
            train_hist = pd.DataFrame(data={"epoch": range(1, n_epochs + 1)})
            for col in ("loss_ae", "loss_classif", "correct", "total"):
                train_hist[f"train_{col}"] = [t[col] for t in train_losses]
            if self.has_valset:
                for col in ("loss_ae", "loss_classif", "correct", "total"):
                    train_hist[f"val_{col}"] = [t[col] for t in val_losses]
            train_hist.to_csv(join(save_dir, "train_history.csv"), index=None)
            # write a json file containing info about the pipeline and networks
            info = {}
            if self.has_valset:
                info["best_epoch"] = best_epoch
                info["best_results"] = val_losses[best_epoch - 1]
            info["l1_pen"] = self.l1_pen
            info["masking"] = self.masking
            info["noise"] = self.noise
            info["pipeline_config"] = {k: v for (k, v) in self.config.items()}
            info["pipeline_config"]["transform_train"] = str(info["pipeline_config"]["transform_train"])
            info["pipeline_config"]["transform_test"] = str(info["pipeline_config"]["transform_test"])
            for net_name, net in self.model.named_children():
                info[f"{net_name}_params"] = net.params
            with open(join(save_dir, "info.json"), "w") as fp:
                json.dump(info, fp)

    def _train(self):
        # Train the model one epoch
        print("Train:")
        self.model.train()
        loss_ae = loss_classif = correct = total = 0
        for batch_idx, batch in enumerate(self.trainloader):
            batch_loss = 0
            self.optimizer.zero_grad()
            out = self._forward(batch)
            if "loss_ae" in out:  # if task is "ae" or "both", add the loss corresponding to the autoencoding task.
                batch_loss += out["loss_ae"]
                loss_ae += out["loss_ae"].item()
            if "loss_classif" in out:  # if task is "classification" or "both", add the corresponding loss.
                batch_loss += out["loss_classif"]
                loss_classif += out["loss_classif"].item()
            batch_loss.backward()
            if (self.diagnose) and (batch_idx == len(self.trainloader) - 1):  # plot gradient flow on the last batch
                plot_grad_flow(self.model.named_parameters())
            self.optimizer.step()
            if self.scheduler:
                self.scheduler.step()
            if "correct" in out:  # keep track of the number of correct samples
                correct += out["correct"]
            total += batch["data"].size(0)
        train_str = ""
        if loss_ae:
            train_str += f"Autoencoder loss: {loss_ae / (batch_idx + 1):.3e} | "
        if loss_classif:
            train_str += f"Classifier loss: {loss_classif / (batch_idx + 1):.3e} | "
        if correct:
            train_str += f"Accuracy: {correct / total:.2%}, ({correct}/{total}) | "
        train_str += f"lr: {self.lr:.3e}"
        print(train_str)
        return {"loss_ae": loss_ae / (batch_idx + 1), "loss_classif": loss_classif / (batch_idx + 1) * 100,
                "correct": correct, "total": total}

    def _forward(self, batch):
        # forward a given batch and calculate losses
        batch["data"], batch["target"] = batch["data"].to(self.device), batch["target"].to(self.device)
        out = {}
        input_data = corrupt_img(batch["data"], self.noise, self.masking)
        hidden = self.encoder.forward(input_data)  # hidden features

        if (self.task == "both") or (self.task == "ae"):
            out_img = self.decoder.forward(hidden)
            out["loss_ae"] = nn.MSELoss()(out_img, batch["data"]) + self.l1_pen * hidden.abs().mean()

        if (self.task == "both") or (self.task == "classification"):
            classes_proba = self.classifier.forward(hidden)
            _, predicted = classes_proba.max(1)
            # loss_classif is divided by 100 to have around the same range as loss_ae while training both
            out["loss_classif"] = nn.CrossEntropyLoss()(classes_proba, batch["target"]) / 100
            out["correct"] = predicted.eq(batch["target"]).sum().item()

        return out

    def _validation(self):
        print("Validation:")
        self.model.eval()
        loss_ae = loss_classif = correct = total = 0
        with torch.no_grad():
            for batch_idx, batch in enumerate(self.valloader):
                out = self._forward(batch)
                if "loss_ae" in out:  # if task is "ae" or "both", add the loss corresponding to the autoencoding task.
                    loss_ae += out["loss_ae"].item()
                if "loss_classif" in out:  # if task is "classification" or "both", add the corresponding loss.
                    loss_classif += out["loss_classif"].item()
                if "correct" in out:
                    correct += out["correct"]
                total += batch["data"].size(0)
        val_str = ""
        if loss_ae:
            val_str += f"Autoencoder loss: {loss_ae / (batch_idx + 1):.3e} | "
        if loss_classif:
            val_str += f"Classifier loss: {loss_classif / (batch_idx + 1):.3e} | "
        if correct:
            val_str += f"Accuracy: {correct / total:.2%}, ({correct}/{total}) | "
        val_str += f"lr: {self.lr:.3e}"
        print(val_str)
        return {"loss_ae": loss_ae / (batch_idx + 1), "loss_classif": loss_classif / (batch_idx + 1) * 100,
                "correct": correct, "total": total}

    def load_net(self, net_name, path):
        """Load a network previously saved.
        
        Parameters
        ----------
        net_name : string, element of ("encoder", "decoder", "classifier")
        path : string (saved network file path)
        """
        state_dict = torch.load(path)
        self.model[net_name].load_state_dict(state_dict)
        self.model[net_name].to(self.device)

    def plot_autoencoder(self, in_img, noise=None, masking=None):
        """Plot the result of an input image fed to the autoencoder.
        
        Parameters
        ----------
        in_img : array or tensor, typically from self.trainset.data ou self.testset.data
        noise : float, optional
            Level of Gaussian noise to apply, by default None (set to self.noise)
        masking : floar, optional
            Proportion of pixels to mask, by default None (set to self.masking)
        """
        self.model.eval()
        if noise is None:
            noise = self.noise
        if masking is None:
            masking = self.masking
        # transpose for displaying with imshow
        original_img = np.array(in_img).transpose((1, 2, 0))
        corrupted_img = corrupt_img(in_img, noise, masking)
        hidden = self.encoder(corrupted_img.to(self.device))
        corrupted_img = np.array(corrupted_img[0]).transpose((1, 2, 0))
        out_img = self.decoder.forward(hidden)
        out_img = out_img.detach().cpu().numpy()[0].transpose((1, 2, 0))

        fig, ax = plt.subplots(1, 3, figsize=(16, 16 / 3))
        ax[0].imshow(original_img)
        ax[0].set_title("Original")
        ax[1].imshow(corrupted_img)
        ax[1].set_title("Corrupted")
        ax[2].imshow(out_img)
        ax[2].set_title("Reconstructed")

        plt.pause(0.05)
        plt.show()

    @property
    def lr(self):
        return self.optimizer.param_groups[0]["lr"]
