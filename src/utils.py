import errno
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch


def corrupt_img(img_tensor, noise=0, masking=0):
    # corrupts an image tensor with a gaussian noise of std `noise` and by setting to 0
    # with a probability of `masking` each of its elements
    img_tensor = img_tensor.clone() # prevent from modifying the original object
    if img_tensor.dim() == 3: # if data is not batched
        img_tensor = img_tensor[None]
    if masking:
        mask = torch.empty_like(img_tensor[:, 0], requires_grad=False).bernoulli_(1 - masking)
        mask = torch.stack((mask, mask, mask), dim=1) # ensures that all channels are set to 0 simultaneously
        img_tensor *= mask
    if noise:
        img_tensor += noise * torch.randn_like(img_tensor, requires_grad=False)
    return img_tensor


def freeze(model):
    model.eval()
    for name, child in model.named_children():
        for param in child.parameters():
            param.requires_grad = False
        freeze(child)


def unfreeze(model):
    model.train()
    for name, child in model.named_children():
        for param in child.parameters():
            param.requires_grad = True
        unfreeze(child)


def create_directory(directory):
    # create a directory and its parent directories if they do not exist.
    try:
        os.makedirs(directory)
    except OSError as e:  # prevent from raising an exception if the directory already exists
        if e.errno != errno.EEXIST:
            raise


def plot_grad_flow(named_parameters):
    # Improved version of what can be found in the following discussion:
    # https://discuss.pytorch.org/t/check-gradient-flow-in-network/15063/10
    '''Plots the gradients flowing through different layers in the net during training.
    Can be used for checking for possible gradient vanishing / exploding problems.

    Usage: Plug this function in Trainer class after loss.backwards() as
    "plot_grad_flow(self.model.named_parameters())" to visualize the gradient flow'''
    flow = pd.DataFrame()
    sns.set(style="white")
    plt.tight_layout()
    for n, p in named_parameters:
        if p.requires_grad and ("bias" not in n) and (p.grad is not None):
            flow = flow.append({"Layer": n, "Gradients": np.asarray(p.grad.cpu().flatten())},
                               ignore_index=True)
    flow = flow.explode("Gradients")
    flow["Gradients"] = flow["Gradients"].astype(float)
    fig, ax = plt.subplots(1, 1, figsize=(16, 8))
    sns.violinplot(x="Layer", y="Gradients", data=flow, ax=ax, cut=1, scale="width")
    plt.xticks(rotation=90)
    plt.pause(0.05)
    plt.show()
