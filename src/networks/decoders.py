from abc import abstractmethod, ABC
from collections import OrderedDict

import torch.nn as nn


class Decoder(nn.Module, ABC):

    def __init__(self, params):
        self.params = params
        super(Decoder, self).__init__()
        width = params["width"]
        self.hidden_size = 4 * width * 4 * 4
        n_convs = params["conv_per_layer"]
        self.cnn = nn.Sequential(OrderedDict([
            ("layer1", self.cnn_layer(4 * width, 2 * width, n_convs)),
            ("layer2", self.cnn_layer(2 * width, width, n_convs)),
            ("layer3", self.cnn_layer(width, 3, n_convs))
        ]))

        if params["dense"]:
            self.dense = nn.Sequential(nn.ReLU(), nn.Linear(self.hidden_size, self.hidden_size))
        else:
            self.dense = nn.Identity()

        self.sigmoid = nn.Sigmoid()

    @abstractmethod
    def cnn_layer(self, in_channels, out_channels, n_conv):
        pass

    def forward(self, hidden):
        hidden = self.dense(hidden)
        hidden = hidden.view(-1, self.hidden_size // 16, 4, 4)
        hidden = self.cnn(hidden)
        out_img = self.sigmoid(hidden)
        return out_img


class StridedDecoder(Decoder):

    def cnn_layer(self, in_channels, out_channels, n_conv):  # n_conv>0
        layer = OrderedDict()
        layer["StridedDeconv"] = nn.ConvTranspose2d(in_channels, in_channels, kernel_size=3,
                                                    stride=2, padding=1, output_padding=1)
        for i in range(n_conv - 1):
            layer[f"Conv{i + 1}"] = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=1)
            layer[f"ReLU{i + 1}"] = nn.ReLU()
            if self.params["BN"]:
                layer[f"BN{i + 1}"] = nn.BatchNorm2d(in_channels)
        layer["LastConv"] = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1)
        if self.params["Dropout"]:
            layer["Dropout"] = nn.Dropout2d(self.params["Dropout"])
        return nn.Sequential(layer)


class UpscaleDecoder(Decoder):

    def cnn_layer(self, in_channels, out_channels, n_conv):  # n_conv>0
        layer = OrderedDict()
        layer["Upsample"] = nn.Upsample(scale_factor=2, mode='nearest')
        for i in range(n_conv - 1):
            layer[f"Conv{i + 1}"] = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=1)
            layer[f"ReLU{i + 1}"] = nn.ReLU()
            if self.params["BN"]:
                layer[f"BN{i + 1}"] = nn.BatchNorm2d(in_channels)
        layer["LastConv"] = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1)
        if self.params["Dropout"]:
            layer["Dropout"] = nn.Dropout2d(self.params["Dropout"])
        return nn.Sequential(layer)
