import torch.nn as nn


class Classifier(nn.Module):
    def __init__(self, params):
        self.params = params
        super(Classifier, self).__init__()
        width = params["width"]
        depth = params["depth"]
        dense = []
        for i in range(depth):
            dense.append(self.dense_layer(width, width))
        dense.append(nn.Linear(width, 10))
        self.dense = nn.Sequential(*dense)

    def dense_layer(self, in_features, out_features):
        layer = [nn.Linear(in_features, out_features)]
        layer.append(nn.ReLU())
        if self.params["BN"]:
            layer.append(nn.BatchNorm1d(out_features))
        if self.params["Dropout"]:
            layer.append(nn.Dropout2d(self.params["Dropout"]))
        return nn.Sequential(layer)

    def forward(self, hidden):
        x = self.dense.forward(hidden)
        return x
