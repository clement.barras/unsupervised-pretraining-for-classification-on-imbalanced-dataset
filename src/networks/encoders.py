from abc import abstractmethod, ABC
from collections import OrderedDict

import torch.nn as nn


class Encoder(nn.Module, ABC):

    def __init__(self, params):
        self.params = params
        super(Encoder, self).__init__()
        width = params["width"]
        n_convs = params["conv_per_layer"]
        self.hidden_size = 4 * width * 4 * 4

        self.cnn = nn.Sequential(OrderedDict([
            ("layer1", self.cnn_layer(3, width, n_convs)),
            ("layer2", self.cnn_layer(width, 2 * width, n_convs)),
            ("layer3", self.cnn_layer(2 * width, 4 * width, n_convs))])
        )

        if params["dense"]:
            self.dense = nn.Sequential(nn.ReLU(), nn.Linear(self.hidden_size, self.hidden_size))
        else:
            self.dense = nn.Identity()

        self.sigmoid = nn.Sigmoid()

    @abstractmethod
    def cnn_layer(self, in_channels, out_channels, n_conv):
        pass

    def forward(self, in_img):
        hidden = self.cnn(in_img)
        hidden = hidden.view(-1, self.hidden_size)
        hidden = self.dense(hidden)
        return hidden


class StridedEncoder(Encoder):

    def cnn_layer(self, in_channels, out_channels, n_conv):
        n_channels = in_channels
        layer = OrderedDict()
        for i in range(n_conv):
            layer[f"Conv{i + 1}"] = nn.Conv2d(n_channels, out_channels, kernel_size=3, padding=1)
            layer[f"ReLU{i + 1}"] = nn.ReLU()
            if self.params["BN"]:
                layer[f"BN{i + 1}"] = nn.BatchNorm2d(out_channels)
            n_channels = out_channels
        layer["StridedConv"] = nn.Conv2d(n_channels, out_channels, kernel_size=3, stride=2, padding=1)
        if self.params["Dropout"]:
            layer["Dropout"] = nn.Dropout2d(self.params["Dropout"])
        return nn.Sequential(layer)


class PoolingEncoder(Encoder):

    def cnn_layer(self, in_channels, out_channels, n_conv):
        n_channels = in_channels
        layer = OrderedDict()
        for i in range(n_conv):
            layer[f"Conv{i + 1}"] = nn.Conv2d(n_channels, out_channels, kernel_size=3, padding=1)
            layer[f"ReLU{i + 1}"] = nn.ReLU()
            if self.params["BN"]:
                layer[f"BN{i + 1}"] = nn.BatchNorm2d(out_channels)
            n_channels = out_channels
        layer["MaxPooling"] = nn.MaxPool2d(2)
        if self.params["Dropout"]:
            layer["Dropout"] = nn.Dropout2d(self.params["Dropout"])
        return nn.Sequential(layer)
